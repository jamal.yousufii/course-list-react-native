import React, { useState } from "react";
import {
  Modal,
  TextInput,
  Button,
  StyleSheet,
  View,
  Alert,
} from "react-native";
import { useCreateCourseMutation } from "../redux/apiSlice";

export const AddCourse = ({ visible, onClose }: any) => {
  const [courseName, setCourseName] = useState("");
  const [courseContent, setCourseContent] = useState("");
  const [createCourse] = useCreateCourseMutation();

  function generateUniqueID() {
    const timestamp = new Date().getTime();
    return `course-${timestamp}`;
  }

  const handleSubmit = () => {
    const formData = {
      id: generateUniqueID(),
      name: courseName,
      content: courseContent,
      completed: false,
    };

    createCourse(formData)
      .unwrap()
      .then(() => {
        Alert.alert("Course added");
        setCourseName("");
        setCourseContent("");
      })
      .catch(() => Alert.alert("there was an error"));
    onClose();
    setCourseName("");
    setCourseContent("");
  };

  return (
    <Modal visible={visible} animationType="slide">
      <View style={styles.modalContainer}>
        <TextInput
          placeholder="Course Name"
          value={courseName}
          onChangeText={setCourseName}
          style={styles.input}
          placeholderTextColor="#000"
        />
        <TextInput
          placeholder="Course Content"
          value={courseContent}
          onChangeText={setCourseContent}
          style={styles.input}
          placeholderTextColor="#000"
        />

        <View style={styles.buttonContainer}>
          <Button title="Submit" onPress={handleSubmit} />
          <Button title="Cancel" onPress={onClose} />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginVertical: 5,
    width: "80%",
  },
  buttonContainer: {
    flexDirection: "row",
    gap: 10,
  },
});
