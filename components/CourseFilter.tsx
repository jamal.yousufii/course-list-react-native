import React, { useState } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import { useDispatch } from "react-redux";
import { isCompleted } from "../redux/courseSlice";

const CourseFilter = () => {
  const [selectedOption, setSelectedOption] = useState<boolean | null>(null);
  const dispatch = useDispatch();

  const handleFilterChange = (value: boolean | null) => {
    setSelectedOption(value);
    dispatch(isCompleted(value));
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => handleFilterChange(null)}
        style={styles.button}
      >
        <Text>All</Text>
        {selectedOption === null && <Text>✔️</Text>}
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleFilterChange(true)}
        style={styles.button}
      >
        <Text>Complete</Text>
        {selectedOption === true && <Text>✔️</Text>}
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => handleFilterChange(false)}
        style={styles.button}
      >
        <Text>Not Complete</Text>
        {selectedOption === false && <Text>✔️</Text>}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    marginVertical: 10,
  },
  button: {
    padding: 10,
    marginHorizontal: 10,
    backgroundColor: "#007AFF",
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
  },
});

export default CourseFilter;
