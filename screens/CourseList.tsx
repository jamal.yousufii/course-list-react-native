import {
  Text,
  FlatList,
  ActivityIndicator,
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  Button,
} from "react-native";
import { useGetCoursesQuery } from "../redux/apiSlice";
import { AddCourse } from "../components/AddCourse";
import { useState } from "react";
import CourseFilter from "../components/CourseFilter";
import { useSelector } from "react-redux";

interface Course {
  id: number;
  name: string;
  content: string;
}

const CourseList = ({ navigation }: any) => {
  const { courseStatus } = useSelector((state: any) => state.course);

  const [modalVisible, setModalVisible] = useState(false);

  const { data, isLoading } = useGetCoursesQuery();

  const handleClose = () => {
    setModalVisible(false);
  };

  const filteredCourses =
    courseStatus === null
      ? data
      : data.filter(
          (course: { completed: boolean }) => course.completed === courseStatus
        );

  if (isLoading) {
    return <ActivityIndicator />;
  }
  const renderItem = ({ item }: { item: Course }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          // Navigate to the course details page with the selected course
          navigation.navigate("CourseDetails", { course: item });
        }}
        style={styles.container}
      >
        <View>
          <Text style={styles.itemContainer}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* add new course  */}
      <Button title="Add Course" onPress={() => setModalVisible(true)} />
      <AddCourse visible={modalVisible} onClose={handleClose} />
      {/* filter course  */}
      <CourseFilter />
      <FlatList
        data={filteredCourses}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </SafeAreaView>
  );
};

export default CourseList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    padding: 20,
    paddingHorizontal: 18,
    marginTop: 8,
    marginLeft: 5,
    marginRight: 5,
    borderWidth: 1,
    borderRadius: 10,
    alignItems: "center",
    gap: 2,
  },
});
