import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import { createStackNavigator } from "@react-navigation/stack";
import CourseList from "./CourseList";
import { CourseDetails } from "./CourseDetails";

function AppWrapper() {
  const Stack = createStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* screen for Course List */}
        <Stack.Screen
          name="CourseList"
          component={CourseList}
          options={{ title: "Course Lists" }}
        />
        {/* screen for Course Details */}
        <Stack.Screen
          name="CourseDetails"
          component={CourseDetails}
          options={{ title: "Course Details" }}
        />
      </Stack.Navigator>
      <StatusBar style="auto" />
    </NavigationContainer>
  );
}

export default AppWrapper;
