import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  Text,
  StyleSheet,
  View,
  SafeAreaView,
  Button,
  Alert,
} from "react-native";
import { useDeleteCourseMutation } from "../redux/apiSlice";

export const CourseDetails = ({ route }: any) => {
  const [deleteCourse] = useDeleteCourseMutation();
  const { course } = route.params;
  const navigation = useNavigation();

  const navigateToHome = () => {
    navigation.navigate("CourseList" as never);
  };
  const handleDeleteCourse = () => {
    deleteCourse(course.id)
      .unwrap()
      .then(() => {
        navigateToHome();
      })
      .catch(() => Alert.alert("there was an error"));
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.itemContainer}>
        {/* <Text>Course Id: {course.id}</Text> */}
        <Text>Course Name: {course.name}</Text>
        <Text>Course Content: {course.content}</Text>
        <Button title="Delete" color="red" onPress={handleDeleteCourse} />
      </View>
      <View style={styles.homeBtn}>
        <Button title="Home" onPress={navigateToHome} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
  },
  itemContainer: {
    flex: 0.9,
    backgroundColor: "#fff",
    padding: 5,
    paddingHorizontal: 18,
    marginTop: 5,
    gap: 2,
  },
  homeBtn: {
    flex: 0.1,
    bottom: 0,
  },
});
