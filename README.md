# React Native Course Catalog App

This is a simple React Native app that displays a list of courses and allows users to view course details.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Project Structure](#project-structure)
- [Usage](#usage)



## Features

- Display a list of courses with their names.
- View detailed information about each course.
- Navigate between the course list and course details.
- Add and Delete course functionality
- Filter the course with "ALL, completed and Not completed" 

## Getting Started

### Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js and npm installed on your machine.
- Expo CLI for React Native development.
- An Android emulator or a physical Android device (for Android development).
- Xcode (for iOS development on a Mac).
- The easit way is to run o physical device (Android or IOS)
- Check this link for installation of Expo go your device 
- To run this app on your mobile device, you will need to have the [Expo Go](https://expo.dev/client) app installed. Follow these steps to get the app running on your device:


### Installation

1. Clone the repository:

````sh
git clone git@gitlab.com:jamal.yousufii/course-list-react-native.git
cd course-list-react-native

## Install the depndecies of the project by the runing the following command 
npm install 

## To start the project run
npm start 

````
A new browser window will open with a QR code. Alternatively, you can find the QR code in the terminal.

Open the Expo Go app on your mobile device.

Use your device's camera to scan the QR code displayed on your computer screen. This will load the app on your mobile device.

The app should now be running on your device, allowing you to interact with it.

Supported Platforms
iOS (iPhone, iPad)
Android (smartphones and tablets)
