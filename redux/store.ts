import { configureStore } from "@reduxjs/toolkit";
import { api } from "./apiSlice";
import courseSlice from "./courseSlice";

export const store = configureStore({
  reducer: {
    [api.reducerPath]: api.reducer,
    course: courseSlice,
    // Add your other reducers here
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(api.middleware),
});

export default store;
