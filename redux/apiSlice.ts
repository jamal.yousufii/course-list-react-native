import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const api = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://course-list-server-rltv.onrender.com/",
  }),
  refetchOnFocus: true,
  refetchOnReconnect: true,
  tagTypes: ["courses"], // Declare a tag type to group related queries/mutations
  endpoints: (builder) => ({
    getCourses: builder.query<any, void>({
      query: () => ({
        url: "/courses",
        method: "GET",
      }),
      providesTags: ["courses"], // associate the result of this mutation with the "courses" tag
    }),
    createCourse: builder.mutation({
      query: (body) => ({
        url: "/courses",
        method: "POST",
        body,
      }),
      invalidatesTags: ["courses"], // invalidate all queries/mutations with the "courses" tag when this mutation is executed
    }),
    deleteCourse: builder.mutation({
      query: (id) => ({
        url: `/courses/${id}`,
        method: "DELETE",
      }),
      invalidatesTags: ["courses"], // invalidate all queries/mutations with the "courses" tag when this mutation is executed
    }),
  }),
});

export const {
  useGetCoursesQuery,
  useCreateCourseMutation,
  useDeleteCourseMutation,
} = api;
