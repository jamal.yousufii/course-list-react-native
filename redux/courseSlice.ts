import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  courseStatus: null,
};

const courseSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    isCompleted: (state, action) => {
      state.courseStatus = action.payload;
    },
    // Add your other reducers here
  },
});

export const { isCompleted } = courseSlice.actions;

export default courseSlice.reducer;
